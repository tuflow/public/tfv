Examples
=========
This page contains a set of examples and guides for tfv. 
To work along with the tutorials please download the `TUFLOW FV Python Toolbox Data Package`_ and check out our `TUFLOW FV Python Toolbox Wiki Page`_.

.. toctree::
    :maxdepth: 1

    :caption: Contents:

    tutorial_01/tutorial_01_tfv_xarray.ipynb
    tutorial_02/tutorial_02_tfv_matplotlib.ipynb
    tutorial_03/tutorial_03_convert_grid.ipynb
    gallery/1.timeseries/timeseries.ipynb
    gallery/2.profiles/profiles.ipynb
    gallery/3.sheets/sheets.ipynb
    gallery/4.curtains/curtains.ipynb
    gallery/5.combined_plots/combined_plots.ipynb
    gallery/6.particles/particles.ipynb
    gallery/7.miscellaneous/miscellaneous.ipynb


.. External Links
.. _TUFLOW FV Python Toolbox Wiki Page: https://fvwiki.tuflow.com/index.php?title=FV_Python_Tools
.. _TUFLOW FV Python Toolbox Data Package: https://downloads.tuflow.com/Utilities/TUFLOW_FV_Python_Toolbox/TUFLOW_FV_Python_Toolbox.zip




