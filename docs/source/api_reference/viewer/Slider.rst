Slider
======
.. toctree::
    :maxdepth: 3

.. autoclass:: tfv.viewer.Slider
    :members:
