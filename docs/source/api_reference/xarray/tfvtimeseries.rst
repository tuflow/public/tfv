TfvTimeseries
==============
.. toctree::
    :maxdepth: 3

.. autoclass:: tfv.xarray.TfvTimeseries
    :members: