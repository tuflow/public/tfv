API Reference
=============

.. toctree::
    :maxdepth: 2

    xarray/index
    extractor/index
    timeseries/index
    visual/index
    viewer/index
    particles/index
    geometry/index