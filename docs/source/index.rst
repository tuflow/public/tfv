.. tfv documentation master file, created by
   sphinx-quickstart on Thu Nov  7 14:06:31 2019.

`tfv` documentation
===========
**tfv** is a suite of tools for post-processing results from `TUFLOW FV`_, a
1D, 2D and 3D hydrodynamic, sediment transport, water quality and particle tracking modelling software. 

The purpose of this page is to serve as the tfv package API Reference. Also provided are links to tutorials
and example datasets.

Installing
----------
The tfv package is available via the Python Package Index `PyPi`_ and `Conda Forge`_.

To install tfv from the conda command line tool::

   $ conda install -c conda-forge tfv
   
.. End of literal block.

Alternatively to install tfv using pip::

   $ python -m pip install tfv
   
.. End of literal block.

*Note: Version |version| has been built and tested on Python 3.9 to 3.11.


Getting Started
---------------
For tips on the setup of Python and tfv, including tutorials and examples please refer to the `TUFLOW FV Python Toolbox Wiki Page`_.

API Reference
-----------------
Class reference documentation

.. toctree::
   :maxdepth: 3

   api_reference/index


Examples
-----------------
Tutorial and example notebooks 

.. toctree::
   :maxdepth: 3

   examples/index
   

Support
-------
For help with the tfv package contact support@tuflow.com


.. External Links
.. _TUFLOW FV: https://www.tuflow.com/Tuflow%20FV.aspx
.. _TUFLOW FV Python Toolbox Wiki Page: https://fvwiki.tuflow.com/index.php?title=FV_Python_Tools
.. _PyPi: https://pypi.org/project/tfv/
.. _Conda Forge: https://github.com/conda-forge/tfv-feedstock











